#include "util.h"
#include <string>
#include <sstream>
#include <iostream>
#include "tokenizer.h"

int nvUtil::readStdinInteger()
{
    int number = 0;
    std::string inputString;
    getline(std::cin, inputString);
    std::stringstream ss(inputString);
    if (!(ss >> number)) number = 0;
    return number;
}

bool nvUtil::readStdinIntegerSequence(std::vector<int> & intSequence)
{
    std::string inputString;
    getline(std::cin, inputString);

    if (inputString.empty()) return false;

    nvTokenizer tokenizer(inputString);
    bool ret = tokenizer.readIntegerSequence(intSequence);
    return ret;
}

bool nvUtil::readStdinCommand(std::string & commandName, std::vector<int> & intSequence)
{
    std::string inputString;
    getline(std::cin, inputString);

    if (inputString.empty()) return false;

    nvTokenizer tokenizer(inputString);

    if (!tokenizer.readIdentifier(commandName)) {
        return false;
    }
    
    if (commandName == SEARCH_SEQUENCE_CMD  || 
        commandName == SS_CMD ||
        commandName == SEARCH_UNORDERED_CMD ||
        commandName == SU_CMD ||
        commandName == SEARCH_BESTMATCH_CMD ||
        commandName == SB_CMD)
    {
        if (!tokenizer.readIntegerSequence(intSequence) && intSequence.empty()) {
            return false;
        }
        return true;
    }
    else if (commandName == QUIT_CMD) {
        return true;
    }
    return false;
}

bool nvUtil::readSearchCommandFromStream(std::ifstream & commandStream, std::string & commandName, std::vector<int> & sequence)  
{
    std::string lineRead;
    getline(commandStream, lineRead);

    nvTokenizer tokenizer(lineRead);

    if (!tokenizer.readIdentifier(commandName)) return false;

    if (!tokenizer.readIntegerSequence(sequence) && sequence.empty()) return false;

    return true;
}

void nvUtil::printSequenceOnStdout(const std::vector<int> & sequence)
{
    for (auto i : sequence) {
        std::cout << i << " ";
    }
}

void nvUtil::countOccurrencesInSequence(const std::vector<int> & sequence, std::map<int, int> & occurenceCountMap)
{
    for ( auto i : sequence) {
        std::map<int, int>::iterator iter = occurenceCountMap.find(i);
        if (iter != occurenceCountMap.end()) {
            iter->second = iter->second + 1;
        }
        else occurenceCountMap.insert(std::make_pair(i, 1));
    }
}


nvHighResTimer::nvHighResTimer(const std::string & timerId) : _id(timerId),
    _totalTime(0),
    _minimumDuration(0x0fffffff),
    _maximumDuration(0)
{
}

void nvHighResTimer::begin()
{
    _refTimePoint = std::chrono::high_resolution_clock::now();
}

void nvHighResTimer::end()
{
    std::chrono::high_resolution_clock::time_point timeNow = std::chrono::high_resolution_clock::now();
    std::chrono::microseconds time_span = std::chrono::duration_cast<std::chrono::microseconds>(timeNow - _refTimePoint);
    std::cout << "\n------------ " << _id << ": " << " duration " <<  time_span.count() << " microseconds" << std::endl;
}

void nvHighResTimer::pause()
{
    std::chrono::high_resolution_clock::time_point timeNow = std::chrono::high_resolution_clock::now();
    std::chrono::microseconds time_span = std::chrono::duration_cast<std::chrono::microseconds>(timeNow - _refTimePoint);

    _totalTime += time_span;
    if (time_span < _minimumDuration) _minimumDuration = time_span;
    if (time_span > _maximumDuration) _maximumDuration = time_span;

  //std::cout << "duration " <<  time_span.count() << " microseconds" << std::endl;
}

std::chrono::microseconds nvHighResTimer::getTotalTime() const
{
    return _totalTime;
}

std::chrono::microseconds nvHighResTimer::getMinimumTime() const
{
    return _minimumDuration;
}

std::chrono::microseconds nvHighResTimer::getMaximumTime() const
{
    return _maximumDuration;
}
