#ifndef _NV_MATRIX_INDEXER_H_
#define _NV_MATRIX_INDEXER_H_

#include <map>
#include <vector>

struct nvRowColumnPair
{
    nvRowColumnPair(unsigned int row, unsigned int column);
    unsigned int     _row;
    unsigned int     _column;
    bool operator< (const nvRowColumnPair & other) const;
};

typedef std::multimap<int, nvRowColumnPair>::const_iterator nvSearchIter ;

class nvMatrixIndexer
{
public:
    void        appendRow(unsigned int rowNum, const std::vector<int> & row);
  //template<typename BidirectionalIterator>
  //std::pair<BidirectionalIterator, BidirectionalIterator>     findValue(int value) const;  
    std::pair< nvSearchIter, nvSearchIter >     findValue(int value) const;  
private:
    std::multimap<int, nvRowColumnPair>     _valueToPositionIndex;

};
#endif
