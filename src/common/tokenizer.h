#ifndef _NV_TOKENIZER_H_
#define _NV_TOKENIZER_H_

#include <string>
#include <vector>

enum class nvTokenType { INVALID, INTEGER, ID };
enum class nvFsmState { START, INTEGER, ID };

class nvTokenizer
{
public:
    nvTokenizer(std::string & parseString);
    bool    readIdentifier(std::string & identifier);
    bool    readIntegerSequence(std::vector<int> & intSequence);

private:
    nvTokenType     _readToken(std::string & token);

private:
    static std::string   _whiteSpaces;
    static std::string   _integers;
    std::string          _parsedString;
    size_t               _currentCharPos;
};

#endif
