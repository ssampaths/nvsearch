#include "exactsearch.h"
#include <iostream>
#include <vector>
#include "matrix.h"


bool nvExactSearch::search(const nvMatrix & matrix, std::vector<int> & resultRows)
{
    if (_searchSequence.size() < 2) return false;
    
    const nvMatrixIndexer * indexer = matrix.getIndexer();

    if (!indexer) return false;

    // Stage 1:
    std::set<int> sequenceSet(_searchSequence.begin(), _searchSequence.end());
    std::map < int, std::pair < nvSearchIter, nvSearchIter > > foundIterMap;
    
    getFoundIterators(sequenceSet, indexer, foundIterMap);
    std::vector< std::set <nvRowColumnPair> > allAdjacentElemPosPairSets; 
    allAdjacentElemPosPairSets.reserve((_searchSequence.size() / 2) + 1);
    std::set<nvRowColumnPair> adjacentElemPosPairSet;

    for (unsigned int s = 0; s < _searchSequence.size() - 1; ++s) {
        adjacentElemPosPairSet.clear();
        getAdjacentRows(foundIterMap[_searchSequence[s]], foundIterMap[_searchSequence[s + 1]],
                adjacentElemPosPairSet);
        if (adjacentElemPosPairSet.size() < 1) {
          //std::cout << "Info: Failed to find sequence" << std::endl;
            return false;
        }
        allAdjacentElemPosPairSets.push_back(adjacentElemPosPairSet);
        // Need to increment s by 2 since the search is done pair-wise.
        // But if the sequence has odd number of integers, the last pair include
        // the second integer of last pair. This increment ensures that.
        if (s != _searchSequence.size() - 3) ++s;
    }

    // Stage 2:
    // Check if all adjacent sets have a common row, if so, that row is a row that the exact search
    // is found.
    return findCommonRows(allAdjacentElemPosPairSets, resultRows);
}

bool nvExactSearch::getAdjacentRows(std::pair<nvSearchIter, nvSearchIter> iterRange1,
                    std::pair<nvSearchIter, nvSearchIter> iterRange2, 
                    std::set<nvRowColumnPair> & adjacentElemPosPairSet)
                    
{
//bool nvExactSearch::getAdjacentRows(foundIterMap[_searchSequence[s]], foundIterMap[_searchSequence[s + 1]])
    nvSearchIter iter1 = iterRange1.first;
    nvSearchIter iter2 = iterRange2.first;

    while (iter1 != iterRange1.second && iter2 != iterRange2.second) {
        if (iter1->second._row == iter2->second._row) {
            if (iter2->second._column - iter1->second._column == 1) {    // check if they are adjacent
                adjacentElemPosPairSet.insert(iter1->second);
                ++iter1; ++iter2;
            }
            else {
                ++iter2;
            }
        }
        else if (iter1->second._row < iter2->second._row) {
            ++iter1;
        }
        else {
            ++iter2;
        }
    }
    return true;
}

bool nvExactSearch::findCommonRows(const std::vector< std::set <nvRowColumnPair> > & allCommonElemPosSets, std::vector<int> & resultRows) 
{
    if (allCommonElemPosSets.empty()) return false;
    std::set<int> resultRowSet;

    if (allCommonElemPosSets.size() == 1) { // only two-int sequence
      //resultRows.insert(resultRows.begin(), allCommonElemPosSets[0].begin(), allCommonElemPosSets[0].end());
        for (auto elemPos : allCommonElemPosSets[0]) {
            resultRowSet.insert(elemPos._row);
        }
        resultRows.insert(resultRows.begin(),  resultRowSet.begin(), resultRowSet.end());
        return  true;
    }

    const std::set <nvRowColumnPair> & firstMatchedElemPosSet = allCommonElemPosSets[0];
    bool bOddSequenceCount = _searchSequence.size() % 2;

    for (auto firstValueRow : firstMatchedElemPosSet) {
        bool allPairsMatched = true;
        nvRowColumnPair nextPairPos(firstValueRow._row, firstValueRow._column);
        for (unsigned int j = 1; j < allCommonElemPosSets.size(); ++j) {
            const std::set<nvRowColumnPair> & nextMatchedRowSet = allCommonElemPosSets[j];
            nextPairPos._column = (bOddSequenceCount && j == allCommonElemPosSets.size() - 1) ?
                                    nextPairPos._column + 1 :
                                    nextPairPos._column + 2;

            if (nextMatchedRowSet.find(nextPairPos) == nextMatchedRowSet.end()) {
                allPairsMatched = false;
                break;
            }
        }

        if (allPairsMatched) resultRowSet.insert(firstValueRow._row);
    }
    resultRows.insert(resultRows.begin(),  resultRowSet.begin(), resultRowSet.end());
    return true;
}
