#include "tokenizer.h"

#include <sstream>
#include <iostream>

std::string nvTokenizer::_whiteSpaces = " \t\r\n";
std::string nvTokenizer::_integers = "0123456789";

nvTokenizer::nvTokenizer(std::string & parseString) : _parsedString(parseString), _currentCharPos(0)
{
}

nvTokenType nvTokenizer::_readToken(std::string & token)
{
    nvTokenType tokenType = nvTokenType::INVALID;
    nvFsmState fsmState = nvFsmState::START;

    char currentChar = ' ';

    while (_currentCharPos != _parsedString.length()) {
        currentChar = _parsedString[_currentCharPos];
        // Start
        if (fsmState == nvFsmState::START) {
            if (_integers.find(currentChar) != std::string::npos) {
                fsmState = nvFsmState::INTEGER;
                tokenType = nvTokenType::INTEGER;
                token.append(1, currentChar);
            } else if (_whiteSpaces.find(currentChar) == std::string::npos) {
                fsmState = nvFsmState::ID;
                tokenType = nvTokenType::ID;
                token.append(1, currentChar);
            }
        }
        // Process Integer
        else if (fsmState == nvFsmState::INTEGER) {
            if (_whiteSpaces.find(currentChar) != std::string::npos) {
                tokenType = nvTokenType::INTEGER;
                break;
            }
            else if (_integers.find(currentChar) == std::string::npos) {
                tokenType = nvTokenType::INVALID;
                token.append(1, currentChar);
                break;
            }
            token.append(1, currentChar);
        }
        // Process Identifier
        else if (fsmState == nvFsmState::ID) {
            if (_whiteSpaces.find(currentChar) != std::string::npos) {
                tokenType = nvTokenType::ID;
                break;
            }
            token.append(1, currentChar);
        }

        ++_currentCharPos;
    }
  //cout << __func__ << "tokenType " << (int)tokenType << endl;
    return tokenType;
}

bool nvTokenizer::readIntegerSequence(std::vector<int> & intSequence)
{
    if (_parsedString.empty()) return false;
    std::string token;
    std::stringstream ss;
    nvTokenType tokenType = nvTokenType::INVALID;

    while (true) {
        token = "";
        tokenType = _readToken(token);

        if (tokenType == nvTokenType::INVALID) {
            return false;
        }

        if (tokenType ==  nvTokenType::INTEGER) {
            ss.str(token);
            ss.clear();
            int value;
            ss >> value;
            intSequence.push_back(value);
          //cout << "Debug: tokenType INTEGER" <<  endl;
        }
    }
    return true;
}

bool nvTokenizer::readIdentifier(std::string & identifier)
{
    nvTokenType tokenType = nvTokenType::INVALID;
    tokenType = _readToken(identifier);

    if (tokenType == nvTokenType::ID) return true;
    return false;
}
