#include "basesearch.h"


void nvBaseSearch::setSequence(const std::vector<int> & sequence)
{
    _searchSequence = sequence;
}

bool nvBaseSearch::getFoundIterators(const std::set<int> & sequenceSet, const nvMatrixIndexer * indexer, std::map < int, std::pair < nvSearchIter, nvSearchIter > > & foundIterMap)
{
    if (!indexer) return false;

    auto iter = sequenceSet.begin();

    for (; iter != sequenceSet.end(); ++iter) {

        foundIterMap[(*iter)] = indexer->findValue((int)*iter);
    }

    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////
//  allCommonRowSet has sets with row numbers. If all the sets has a common number,
//  that will be added to resultRows 
//  ex. If s1, s2 and s3 are sets such that s1 = {0, 1, 2, 4, 6}  s2 = {1, 2, 6}  s3 = {0, 1, 2}
//  then, resultRows = {1, 2}
////////////////////////////////////////////////////////////////////////////////////////////////
bool nvBaseSearch::findCommonRows(const std::vector< std::set <int> > & allCommonRowSet, std::vector<int> & resultRows) 
{
    if (allCommonRowSet.empty()) return false;

    if (allCommonRowSet.size() == 1) { // only two-int sequence
        resultRows.insert(resultRows.begin(), allCommonRowSet[0].begin(), allCommonRowSet[0].end());
        return  true;
    }

    const std::set <int> & firstMatchedRowSet = allCommonRowSet[0];

    for (auto firstRow : firstMatchedRowSet) {
        bool allPairsMatched = true;
        for (unsigned int j = 1; j < allCommonRowSet.size(); ++j) {
            const std::set<int> & nextMatchedRowSet = allCommonRowSet[j];
            if (nextMatchedRowSet.find(firstRow) == nextMatchedRowSet.end()) {
                allPairsMatched = false;
                break;
            }
        }

        if (allPairsMatched) resultRows.push_back(firstRow);
    }

    return true;
}
