#include "diskaccess.h"
#include <iostream>
#include "matrix.h"

nvBinaryFormatDiskAccess::nvBinaryFormatDiskAccess(const char * fileName)
{
    _fileStream.open(fileName, std::ios::in | std::ios::out | std::ios::binary);

    if (_fileStream.fail()) {
        std::cout << "Error: Cannot open " << fileName << " for writing" << std::endl;
        _fileStream.close();
    }
}

nvBinaryFormatDiskAccess::~nvBinaryFormatDiskAccess()
{
}

bool nvBinaryFormatDiskAccess::writeMatrix(const nvMatrix & matrix)
{
    if (_fileStream.fail()) return false;

    unsigned int rowCount = matrix.getRowCount();
    unsigned int columnCount = matrix.getColumnCount();

    _fileStream.write((char *)&rowCount, sizeof(rowCount));
    _fileStream.write((char *)&columnCount, sizeof(columnCount));

    for (unsigned int r = 0; r < rowCount; ++r) {
        for (unsigned int c = 0; c < columnCount; ++c) {
            int element = matrix(r, c);
            _fileStream.write((char *)&element, sizeof(int));
        }
    }
    return true;
}

std::unique_ptr<nvMatrix> nvBinaryFormatDiskAccess::readMatrix(bool enableIndexing, bool dump)
{
    if (_fileStream.fail()) return nullptr;

    unsigned int rowCount = 0;
    unsigned int columnCount = 0;
    _fileStream.seekg(0);

    if (!_fileStream.read((char *)&rowCount, sizeof(int))) {
        return nullptr;
    }

    if (!_fileStream.read((char *)&columnCount, sizeof(int))) {
        return nullptr;
    }

    if (rowCount == 0 || columnCount == 0) return nullptr;

    std::unique_ptr<nvMatrix> matrix(new nvMatrix(rowCount, columnCount, enableIndexing));
    std::vector<int> rowData;

    if (dump) std::cout << std::endl;

    for (unsigned int r = 0; r < rowCount; ++r) {
        rowData.clear();
        if (!readSequence(columnCount, rowData)) {
            return std::unique_ptr<nvMatrix>(nullptr);
        }
        matrix->appendRow(rowData);

        if (dump) {
            for (unsigned int c = 0; c < rowData.size(); ++c) {
                std::cout << rowData[c] << " ";
            }
            std::cout << std::endl;
        }
    }

    return matrix;
}

bool nvBinaryFormatDiskAccess::readSequence(int size, std::vector<int> & sequence)
{
    if (_fileStream.fail()) return false;

    int rowBuffer [size];

    if  (_fileStream.read((char *)rowBuffer, size * sizeof(int))) {
        sequence.insert(sequence.begin(), rowBuffer, rowBuffer + size);
        return true;
    }
    return false;
}
