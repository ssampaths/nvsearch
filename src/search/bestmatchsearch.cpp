#include "bestmatchsearch.h"
#include <iostream>
#include "matrix.h"
#include "util.h"

bool nvBestmatchSearch::search(const nvMatrix & matrix, std::vector<int> & resultRows)
{
    if (_searchSequence.size() < 2) return false;
    (void) resultRows;
    
    const nvMatrixIndexer * indexer = matrix.getIndexer();

    if (!indexer) return false;

    // Stage 1:
    std::set<int> sequenceSet(_searchSequence.begin(), _searchSequence.end());
    std::map < int, std::pair < nvSearchIter, nvSearchIter > > foundIterMap;
    getFoundIterators(sequenceSet, indexer, foundIterMap);
    std::map < int, int > sequenceNumToCountMap;
    std::map <int, int> rowNumToOccurrencesMap;
    std::vector<std::map <int, int> > allRowNumToOccurrencesMaps;
    std::vector<int> uniqueSequenceElements;

    nvUtil::countOccurrencesInSequence(_searchSequence, sequenceNumToCountMap);

    for (auto seqElement : sequenceNumToCountMap) {
        rowNumToOccurrencesMap.clear();
        findOccurrencesInSameRow(foundIterMap[seqElement.first], seqElement.second, rowNumToOccurrencesMap);
        allRowNumToOccurrencesMaps.push_back(rowNumToOccurrencesMap);
        uniqueSequenceElements.push_back(seqElement.first);
      ////XXX debuggin
      //for (auto m : rowNumToOccurrencesMap) {
      //    std::cout << " seq: " << seqElement.first << " rowNum " << m.first << " count " << m.second << std::endl;
      //}
    }

    unsigned int bestMatchRow = 0;
    unsigned int maxMatchCount = 0;

    for (unsigned int s = 0; s < allRowNumToOccurrencesMaps.size(); ++s) {
        const std::map<int, int> & currentRowToCountMap = allRowNumToOccurrencesMaps[s];

        for (auto rowCount : currentRowToCountMap) {
            unsigned int currentRow = rowCount.first;
            int expectedCount = sequenceNumToCountMap[uniqueSequenceElements[s]]; 
            unsigned int totalOccurrenceCount = (expectedCount < rowCount.second) ? expectedCount : rowCount.second;

            for (unsigned int k = s + 1; k < allRowNumToOccurrencesMaps.size(); ++k) {
                const std::map<int, int> & targetMap = allRowNumToOccurrencesMaps[k];
                std::map<int, int>::const_iterator findIter = targetMap.find(currentRow);
                if (findIter != targetMap.end()) {
                    // If the found occurrences in the row is greater than 
                    // what is expected in the search sequence, increment it with expected count.
                    // Else, increment with real count found in the row
                    expectedCount = sequenceNumToCountMap[uniqueSequenceElements[k]]; 
                    totalOccurrenceCount += (expectedCount < findIter->second) ? expectedCount : findIter->second;
                }
            }
            if (totalOccurrenceCount > maxMatchCount) {
                maxMatchCount = totalOccurrenceCount;
                bestMatchRow = currentRow;
            }
        }
    }

    resultRows.push_back(bestMatchRow);
    return true;
}


void nvBestmatchSearch::findOccurrencesInSameRow(std::pair<nvSearchIter, nvSearchIter> iterRange, int numOccurrencesExpected, std::map<int, int> & rowNumToOccurrencesMap)
{
    nvSearchIter iter = iterRange.first;
    if (iter == iterRange.second) return;

    unsigned int nCurrentRow = iter->second._row;
    int nActualOccurrences = 0;

    while (iter != iterRange.second) {
        if (nCurrentRow == iter->second._row) {
            ++nActualOccurrences;
            ++iter;
            continue;
        }
        if (nActualOccurrences) rowNumToOccurrencesMap.insert(std::make_pair(nCurrentRow, nActualOccurrences >= numOccurrencesExpected ? numOccurrencesExpected : nActualOccurrences));
        nCurrentRow = iter->second._row;
        nActualOccurrences = 0;
    }

    if  (nActualOccurrences) rowNumToOccurrencesMap.insert(std::make_pair(nCurrentRow, nActualOccurrences));
}
