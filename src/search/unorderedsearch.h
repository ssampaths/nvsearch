#ifndef _NV_UNORDERED_SEARCH_H_
#define _NV_UNORDERED_SEARCH_H_

#include "basesearch.h"

class nvUnorderedSearch : public nvBaseSearch
{
public:
    virtual bool    search(const nvMatrix & matrix, std::vector<int> & resultRows) final override;
    bool            getSameRowSet(std::pair<nvSearchIter, nvSearchIter> & iterRange1,
                        std::pair<nvSearchIter, nvSearchIter> & iterRange2, 
                        int nOccurrences1, int nOccurrences2, std::set<int> & sameRowSet);
    bool            getSameRowSet(std::pair<nvSearchIter, nvSearchIter> & iterRange, 
                        int nOccurrences, std::set<int> & sameRowSet);
    bool            findOccurrencesInSameRow(std::pair<nvSearchIter, nvSearchIter> & valueIterRange, int count);

private:

};


#endif
