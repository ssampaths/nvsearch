#ifndef _NV_BASE_SEARCH_H
#define _NV_BASE_SEARCH_H

#include <vector>
#include <map>
#include <set>
#include "matrixIndexer.h"

class nvMatrix;
class nvMatrixIndexer;

class nvBaseSearch
{
public:
    void            setSequence(const std::vector<int> & sequence);
    virtual bool    search(const nvMatrix & matrix, std::vector<int> & resultRows) = 0;
    bool            getFoundIterators(const std::set<int> & sequenceSet, const nvMatrixIndexer * indexer, 
                        std::map < int, std::pair < nvSearchIter, nvSearchIter> > & foundIterMap);
    bool            findCommonRows(const std::vector< std::set <int> > & allSameRowsSet, std::vector<int> & resultRows); 

protected:
    std::vector<int>     _searchSequence;
private:
};


#endif
