#ifndef _NV_BEST_MATCH_SEARCH_H_
#define _NV_BEST_MATCH_SEARCH_H_

#include "basesearch.h"
#include <map>

class nvBestmatchSearch : public nvBaseSearch
{
public:
    virtual bool    search(const nvMatrix & matrix, std::vector<int> & resultRows) final override;

    void            findOccurrencesInSameRow(std::pair<nvSearchIter, nvSearchIter> iterRange, int numOccurrencesExpected, std::map<int, int> & rowNumToOccurrencesMap);

};
#endif
