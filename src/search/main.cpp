#include <iostream>
#include <fstream>
#include <unistd.h>
#include <random>
#include "matrix.h"
#include "matrixIndexer.h"
#include "diskaccess.h"
#include "util.h"
#include "exactsearch.h"
#include "unorderedsearch.h"
#include "bestmatchsearch.h"

void startInteractiveSearch(const char * matrixFileName, bool dumpMatrixOnScreen);
void startSearch(const char * matrixFileName, const char * commandFileName, bool dumpMatrixOnScreen);
bool readCommand(std::string & command, std::vector<int> & sequence);
void processCommand(const nvMatrix & matrix, const std::string & command, const std::vector<int> & sequence);
void runBenchmark();
void initMatrixValues(nvMatrix & matrix);
void searchMatrix(const nvMatrix & matrix);
void doSearch(const nvMatrix & matrix, nvBaseSearch & searchObj);
void createRandomSearchPattern(std::vector<int> & searchPattern);

int main(int argc, char ** argv)
{
    if (argc < 2) {
        std::cout << " Usage: " << argv[0] << " [-db] MATRIX_FILE [SEQUENCE_FILE]"<< std::endl;
        std::cout << "\tOPTIONS: -d     Dump matrix on screen while loading." << std::endl;
        std::cout << "\t         -b     Run benchmark." << std::endl;
        return 1;
    }

    bool bDumpMatrixOnScreen = false;
    bool bBenchmark = false;
    char c;

    while ((c = getopt (argc, argv, "bd")) != -1)
    switch (c)
    {
        case 'd':
            bDumpMatrixOnScreen = true;
            break;
        case 'b':
            bBenchmark = true;
            break;
        case '?':
            if (isprint(optopt))
                fprintf (stderr, "Unknown option `-%c'.\n", optopt);
            else
                fprintf (stderr,
                        "Unknown option character `\\x%x'.\n",
                        optopt);
            return 1;
        default:
            abort ();
    }

    int nonOptArgCount = 0;
    const char * matrixFileName = nullptr;
    const char * commandFileName = nullptr;

    for (int index = optind; index < argc; index++) {
        if (nonOptArgCount == 0) matrixFileName = argv[index];
        else if (nonOptArgCount == 1) commandFileName = argv[index];
        ++nonOptArgCount;
        printf ("Non-option argument %s\n", argv[index]);
    }

    if (bBenchmark) {
        runBenchmark();
        return 0;
    }
       
    std::ifstream mfile;
    mfile.open(matrixFileName, std::ifstream::in | std::ifstream::binary);
    if (!mfile.good()) {
        std::cout << "Error: Cannot open file \'" << matrixFileName << "\' for reading" << std::endl;
        return 2;
    }
    mfile.close();

    if (commandFileName) {
        std::ifstream sfile;
        sfile.open(commandFileName, std::ifstream::in);
        if (!sfile.good()) {
            std::cout << "Error: Cannot open file \'" << matrixFileName << "\' for reading" << std::endl;
            return 2;
        }
        sfile.close();

        startSearch(matrixFileName, commandFileName, bDumpMatrixOnScreen);
        return 0;
    }

    startInteractiveSearch(matrixFileName, bDumpMatrixOnScreen);

    return 0;
}

void startInteractiveSearch(const char * matrixFileName, bool dumpMatrixOnScreen)
{
    nvBinaryFormatDiskAccess diskAccess(matrixFileName);
    std::cout << "Info: Reading matrix file \'" << matrixFileName << "\'" << std::endl;
    nvHighResTimer timer("readMatrix");
    timer.begin();
    std::unique_ptr<nvMatrix> matrix = diskAccess.readMatrix(true, dumpMatrixOnScreen);  // index, dump
    timer.end();

    if (!matrix) {
        std::cout << "Error: Reading matrix failed" << std::endl;
        return;
    }

    std::cout << "Info: Done.\n" << std::endl;
    std::vector<int> sequence;
    std::string command;

    while (readCommand(command, sequence)) {
        if (command == QUIT_CMD) return;

        processCommand(*matrix, command, sequence);
        sequence.clear();
        command = "";
        std::cout << std::endl;
    }

    std::cout << "\nInfo: Search completed successfully" << std::endl;
}

bool readCommand(std::string & command, std::vector<int> & sequence)
{
    std::cout << "command> ";
    return nvUtil::readStdinCommand(command, sequence);
}

void processCommand(const nvMatrix & matrix, const std::string & command, const std::vector<int> & sequence)
{
    std::vector<int> matchedRows;

    if (command == SEARCH_SEQUENCE_CMD || command == SS_CMD) {
        nvExactSearch searchObj;
        searchObj.setSequence(sequence);
        nvHighResTimer timer("searchSequence");
        timer.begin();
        searchObj.search(matrix, matchedRows);
        timer.end();
    }
    else if (command == SEARCH_UNORDERED_CMD || command == SU_CMD) {
        nvUnorderedSearch searchObj;
        searchObj.setSequence(sequence);
        nvHighResTimer timer("searchUnordered");
        timer.begin();
        searchObj.search(matrix, matchedRows);
        timer.end();
    }
    else if (command == SEARCH_BESTMATCH_CMD || command == SB_CMD) {
        nvBestmatchSearch searchObj;
        searchObj.setSequence(sequence);
        nvHighResTimer timer("searchBestMatch");
        timer.begin();
        searchObj.search(matrix, matchedRows);
        timer.end();
    }

    if (matchedRows.empty()) {
        std::cout << "\nInfo: Sequence ";
        nvUtil::printSequenceOnStdout(sequence);
        std::cout << ". No matches found" << std::endl;
        return;
    }

    std::cout << "\nSequence ";
    nvUtil::printSequenceOnStdout(sequence);
    std::cout << " found in following rows:" << std::endl;

    for (auto row : matchedRows) {
        std::cout << row << " ";
    }
}

void startSearch(const char * matrixFileName, const char * commandFileName, bool dumpMatrixOnScreen)
{
    nvBinaryFormatDiskAccess matrixFileAccess(matrixFileName);
    std::cout << "Info: Reading matrix file \'" << matrixFileName << "\'" << std::endl;
    nvHighResTimer timer("readMatrix");
    timer.begin();
    std::unique_ptr<nvMatrix> matrix = matrixFileAccess.readMatrix(true, dumpMatrixOnScreen);  // index, dump
    timer.end();

    if (!matrix) {
        std::cout << "Error: Reading matrix failed" << std::endl;
        return;
    }

    std::cout << "Info: Done.\n" << std::endl;
    std::cout << "Info: Reading command file \'" << commandFileName << "\'" << std::endl;
    std::ifstream commandStream;
    commandStream.open(commandFileName);

    if (commandStream.fail()) {
        std::cout << "Error: Reading command file failed" << std::endl;
        return;
    }

    std::string command;
    std::vector<int> sequence;

    while (nvUtil::readSearchCommandFromStream(commandStream, command, sequence)) {
        processCommand(*matrix, command, sequence);
        command = "";
        sequence.clear();
    }

    std::cout << "\nInfo: Search completed successfully" << std::endl;
}

void runBenchmark()
{
    {
        nvMatrix m1000x1000(1000, 1000, true);
        std::cout << "--- Create matrix 1000x1000" << std::endl;
        initMatrixValues(m1000x1000);
        searchMatrix(m1000x1000);
    }

    {
        nvMatrix m1000x10000(1000, 10000, true);
        std::cout << "\n--- Create matrix 1000x10000" << std::endl;
        initMatrixValues(m1000x10000);
        searchMatrix(m1000x10000);
    }

    {
        nvMatrix m10000x1000(10000, 1000, true);
        std::cout << "\n--- Create matrix 10000x1000" << std::endl;
        initMatrixValues(m10000x1000);
        searchMatrix(m10000x1000);
    }

    {
        nvMatrix m10000x10000(10000, 10000, true);
        std::cout << "\n--- Create matrix 10000x10000" << std::endl;
        initMatrixValues(m10000x10000);
        searchMatrix(m10000x10000);
    }
}

void  initMatrixValues(nvMatrix & matrix)
{
    std::random_device rd;
    std::mt19937 eng(rd());
    std::uniform_int_distribution<> distr;  // positive int range
    std::vector<int> row;
    row.reserve(matrix.getColumnCount());

    for (unsigned int r = 0; r < matrix.getRowCount(); ++r) {
        for (unsigned int c = 0; c < matrix.getColumnCount(); ++c) {
            row.push_back(distr(eng));
          //std::cout << " " << row.back();
        }
        matrix.appendRow(row);
        row.clear();
    }
}

void searchMatrix(const nvMatrix & matrix)
{
    std::cout << "--- Benchmark matrix " << matrix.getRowCount() << "X" << matrix.getColumnCount() << " --" << std::endl;
    nvExactSearch orderedSearchObj;
    std::cout << " -- Ordered Search" << std::endl;
    doSearch(matrix, orderedSearchObj);

    nvUnorderedSearch unorderedSearchObj;
    std::cout << " -- Unordered Search" << std::endl;
    doSearch(matrix, unorderedSearchObj);

    nvBestmatchSearch bestMatchSearchObj;
    std::cout << " -- Best-match Search" << std::endl;
    doSearch(matrix, bestMatchSearchObj);
}

void doSearch(const nvMatrix & matrix, nvBaseSearch & searchObj)
{
    nvHighResTimer timer("bmsearch");
    std::vector<int> searchPattern;
    std::vector<int> resultSet;
    searchPattern.reserve(20);

    for (int pCount = 0; pCount < 1000; ++pCount) {
        createRandomSearchPattern(searchPattern);
        searchObj.setSequence(searchPattern);
        timer.begin();
        searchObj.search(matrix, resultSet);
        timer.pause();
    }

    std::cout << "Stat: Minimum " << timer.getMinimumTime().count() << 
                 " Maximum " << timer.getMaximumTime().count() << 
                 " Total " << timer.getTotalTime().count() <<
                 " Average " << timer.getTotalTime().count() / 1000.0 << 
                 " (microseconds)" <<
                 std::endl;
}

void createRandomSearchPattern(std::vector<int> & searchPattern)
{
    std::random_device rd;
    std::mt19937 eng(rd());
    std::uniform_int_distribution<> distr;  // positive int range
    searchPattern.clear();

    for (unsigned int c = 0; c < 20; ++c) {
        searchPattern.push_back(distr(eng));
    }
}
