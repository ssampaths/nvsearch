#include "unorderedsearch.h"
#include <iostream> // remove later
#include "matrix.h"
#include "util.h"

bool nvUnorderedSearch::search(const nvMatrix & matrix, std::vector<int> & resultRows)
{
    if (_searchSequence.size() < 2) return false;
    (void) resultRows;
    
    const nvMatrixIndexer * indexer = matrix.getIndexer();

    if (!indexer) return false;

    // Stage 1:
    std::set<int> sequenceSet(_searchSequence.begin(), _searchSequence.end());
    std::map < int, std::pair < nvSearchIter, nvSearchIter > > foundIterMap;
    getFoundIterators(sequenceSet, indexer, foundIterMap);
    std::map < int, int > sequenceNumToCountMap;
    nvUtil::countOccurrencesInSequence(_searchSequence, sequenceNumToCountMap);
    std::vector< std::set <int> > allSameRowsSet; 
    allSameRowsSet.reserve((sequenceSet.size() / 2) + 1);
    std::set<int> sameRowSet;

    std::map<int, int>::iterator seqIter = sequenceNumToCountMap.begin();
    unsigned int s = 0;
    // if sequence has only on unique number
    if (sequenceNumToCountMap.size() < 2) { 
        getSameRowSet(foundIterMap[seqIter->first], seqIter->second, sameRowSet);
        allSameRowsSet.push_back(sameRowSet);
    }
    else {
        while (s < (sequenceNumToCountMap.size() - 1) && seqIter != sequenceNumToCountMap.end()) {
            sameRowSet.clear();
            int nRepeatCount1 = seqIter->second;
            std::pair<nvSearchIter, nvSearchIter> iterRange1 = foundIterMap[seqIter->first];
            ++seqIter; ++s;

            int nRepeatCount2 = seqIter->second;
            std::pair<nvSearchIter, nvSearchIter> iterRange2 = foundIterMap[seqIter->first];
            getSameRowSet(iterRange1, iterRange2, nRepeatCount1, nRepeatCount2, sameRowSet); 

            if (sameRowSet.empty()) return false;
            allSameRowsSet.push_back(sameRowSet);

            // For odd number of unique items, last pair is made up of already searcned value
            // For regular case, need to increment twice.
            if (s != sequenceNumToCountMap.size() - 2) {    // if last-two item
                ++seqIter; ++s; 
            }
        }
    }
    // Stage 2:
    // Check if all same-row sets have a common row, if so, that row is a row that has the unordered sequence is found.
    return findCommonRows(allSameRowsSet, resultRows);
}

bool nvUnorderedSearch::getSameRowSet(std::pair<nvSearchIter, nvSearchIter> & iterRange1,
                    std::pair<nvSearchIter, nvSearchIter> & iterRange2, 
                    int nOccurrences1, int nOccurrences2, std::set<int> & sameRowSet)
                    
{
//bool nvExactSearch::getAdjacentRows(foundIterMap[_searchSequence[s]], foundIterMap[_searchSequence[s + 1]])
    nvSearchIter & iter1 = iterRange1.first;
    nvSearchIter & iter2 = iterRange2.first;

    while (iter1 != iterRange1.second && iter2 != iterRange2.second) {
        if (iter1->second._row == iter2->second._row) {
            // need to cache row as findOccurrencesInSameRow() calls alter the iter
            unsigned int row = iter1->second._row; 
            if (findOccurrencesInSameRow(iterRange1, nOccurrences1) &&
                findOccurrencesInSameRow(iterRange2, nOccurrences2)) {
                sameRowSet.insert(row);
            }
            // no need to ++ iterators in this scope since findOccurrencesInSameRow() do that
        }
        else if (iter1->second._row < iter2->second._row) {
            ++iter1;
        }
        else {
            ++iter2;
        }
    }
    return true;
}
// single value version. Called when search pattern is in the form x x x x .... x
bool nvUnorderedSearch::getSameRowSet(std::pair<nvSearchIter, nvSearchIter> & iterRange, 
        int nOccurrences, std::set<int> & sameRowSet)
                    
{
    nvSearchIter & iter = iterRange.first;

    while (iter != iterRange.second) {
        unsigned currentRow = iter->second._row;
        if (findOccurrencesInSameRow(iterRange, nOccurrences)) 
            sameRowSet.insert(currentRow);
    }
    return true;
}

bool nvUnorderedSearch::findOccurrencesInSameRow(std::pair<nvSearchIter, nvSearchIter> & valueIterRange, int count)
{
    nvSearchIter & iter = valueIterRange.first;
    // no value matches in the matrix
    if (iter == valueIterRange.second) return false;

    int nOccurrences = 0;
    unsigned int currentRow = iter->second._row;

    while (iter != valueIterRange.second && nOccurrences < count) {
        if (iter->second._row == currentRow) {
            ++nOccurrences;
            ++iter;
        }
        else return false;
    }
    return (nOccurrences == count);
}
