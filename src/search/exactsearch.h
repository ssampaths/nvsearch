#ifndef _NV_EXACT_SEARCH_H_
#define _NV_EXACT_SEARCH_H_

#include <set>
#include "basesearch.h"
#include "matrixIndexer.h"

class nvExactSearch : public nvBaseSearch
{
public:
    virtual bool    search(const nvMatrix & matrix, std::vector<int> & resultRows) final override;
    bool            getAdjacentRows(std::pair<nvSearchIter, nvSearchIter> iterRange1, 
                        std::pair<nvSearchIter, nvSearchIter> iterRange2, std::set<nvRowColumnPair> & adjacentRowsSet);
    bool            findCommonRows(const std::vector< std::set <nvRowColumnPair> > & allCommonElemPosSets, std::vector<int> & resultRows); 
private:

};
#endif
