#ifndef _NV_MATRIX_H_
#define _NV_MATRIX_H_

#include <vector>

class nvMatrixIndexer;

class nvMatrix
{
public:
    nvMatrix(int rowCount, int columnCount, bool enableIndexing);
    ~nvMatrix();
    int     operator()(int row, int column) const;
    void    setDimension(int nRows, int nColumns);
    bool    appendRow(const std::vector<int> & row);
    unsigned int    getRowCount() const;
    unsigned int    getColumnCount() const;
    const nvMatrixIndexer *   getIndexer() const;

private:
    std::vector<int>    _elements;
    nvMatrixIndexer *   _indexer;
    unsigned int    _rowCount;
    unsigned int    _columnCount;
    unsigned int    _currentRow;
};
#endif
