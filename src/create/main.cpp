#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <random>
#include <memory>
#include "util.h"
#include "matrix.h"
#include "diskaccess.h"

bool readUserInput(const char * fileName);
bool createBenchmarkMatrix(const char * fileName);
std::unique_ptr<nvMatrix> readMatrix(unsigned int nRows, unsigned int nCols);

int main(int argc, char ** argv)
{
    if (argc < 2) {
        std::cout << "Usage: " << argv[0] << " [-b] OUTPUT_FILENAME" << std::endl;
        std::cout << "\tOPTIONS: -b     Create benchmark matrix for given size" << std::endl;
        return 1;
    }
    //
    bool bCreateBenchmarkMatrix = false;

    if (argc == 3) {
        std::string op("-b");
        if (op.compare(argv[1]) != 0) {
            std::cout << "Error: Invalid option \'" << argv[1] << "\'" << std::endl;
            return 2;
        }
        bCreateBenchmarkMatrix = true;
    }

    const char * fileName = (argc == 3) ? argv[2] : argv[1];
    std::ofstream file;
    file.open(fileName, std::ios::out | std::ios::binary);

    if (file.fail()) {
        std::cout << "Error: Cannot open " << fileName << " for writing" << std::endl;
        return 3;
    }
    file.close();

    if (bCreateBenchmarkMatrix) {
        if (!createBenchmarkMatrix(fileName)) return 4;
        return 0;
    }

    while (!readUserInput(fileName)) {
    }

    return 0;
}

bool readRowCount(unsigned int & rowCount)
{
    std::cout << "Number of rows > ";
    rowCount = nvUtil::readStdinInteger();
    if (rowCount < 1) {
        std::cout << "Error: Invalid row size" << std::endl;
        return false;
    }
    return true;
}

bool readColumnCount(unsigned int & columnCount)
{
    std::cout << "Number of columns > ";
    columnCount = nvUtil::readStdinInteger();
    if (columnCount < 1) {
        std::cout << "Error: Invalid column size" << std::endl;
        return false;
    }
    return true;
}

bool readUserInput(const char * fileName)
{
    std::cout << "Please enter matrix dimensions:" << std::endl;
    unsigned int nRows = 0;
    unsigned int nColumns = 0;

    while (!readRowCount(nRows)) {}

    while (!readColumnCount(nColumns)) {}

    std::unique_ptr<nvMatrix> matrix = readMatrix(nRows, nColumns);
    if (!matrix) return false;

    nvBinaryFormatDiskAccess diskFile(fileName);
    if (!diskFile.writeMatrix(*matrix)) {
        std::cout << "Error: Failed  to write matrix to file" << std::endl;
        return false;
    }

    //XXX For debugging. Remove this
    std::cout << "|" << std::endl << "|" << std::endl << "|" << std::endl;

    std::unique_ptr<nvMatrix> m = diskFile.readMatrix();

    if (m) {
        std::cout << "nRows " << m->getRowCount() << " nCols " << m->getColumnCount() << std::endl;
        for (unsigned int r = 0; r < m->getRowCount(); ++r) {
            for (unsigned int c = 0; c < m->getColumnCount(); ++c) {
                std::cout << (*m)(r, c) << " ";
            }
            std::cout << std::endl;
        }
    }

    return true;
}

std::unique_ptr<nvMatrix> readMatrix(unsigned int nRows, unsigned int nCols)
{
    std::vector<int> integerSequence;
    unsigned int i = 0;
    std::unique_ptr<nvMatrix> matrix(new nvMatrix(nRows, nCols, false));

    while (i < nRows) {
        std::cout << "Enter row " << i << " elements > ";
        if (!nvUtil::readStdinIntegerSequence(integerSequence)) {
            if (integerSequence.size() != nCols) {
                std::cout << "Error: invalid or incompatible integer sequence" << std::endl;
                integerSequence.clear();
                continue;
            }
        }
        ++i;
        // append to matrix
        if (!matrix->appendRow(integerSequence)) {
            std::cout << __func__ << "append failed" <<  std::endl;
            return std::unique_ptr<nvMatrix>(nullptr);
        }
        integerSequence.clear();
    }
    return matrix;
}

bool createBenchmarkMatrix(const char * fileName)
{
    std::cout << "Please enter matrix dimensions:" << std::endl;
    unsigned int nRows = 0;
    unsigned int nColumns = 0;

    while (!readRowCount(nRows)) {}

    while (!readColumnCount(nColumns)) {}

    nvMatrix matrix(nRows, nColumns, false);
    std::vector<int> row;
    row.reserve(nColumns);
    std::random_device rd;
    std::mt19937 eng(rd());
    std::uniform_int_distribution<> distr(10, 30); // random integers of range

    for (unsigned int r = 0; r < nRows; ++r) {
        for (unsigned int c = 0; c < nColumns; ++c) {
            row.push_back(distr(eng));
        }
        matrix.appendRow(row);
        row.clear();
    }

    nvBinaryFormatDiskAccess diskWriter(fileName);
    if (!diskWriter.writeMatrix(matrix)) {
        std::cout << "Error: Failed to write matrix" << std::endl;
        return false;
    }

    // generate a set of search pattern as well and write to a text file
    std::string searchCommandFile(fileName);
    searchCommandFile += "-search.txt";
    std::ofstream searchFile(searchCommandFile);
    std::stringstream ss;
    for (int i = 0; i < 20; ++i) {  // 20 searchUnordered commands
        ss << "su";
        for (int j = 0; j < 20; ++j) {
            ss << " " << distr(eng);
        }
        ss << "\n";
    }

    searchFile << ss.str();
    searchFile.close();
    return true;
}
