#ifndef _NV_UTIL_H_
#define _NV_UTIL_H_

#include <vector>
#include <string>
#include <fstream>
#include <map>
#include <chrono>

#define SEARCH_SEQUENCE_CMD    "searchSequence"
#define SS_CMD                  "ss"  // shortened forms
#define SEARCH_UNORDERED_CMD   "searchUnordered"
#define SU_CMD                  "su"
#define SEARCH_BESTMATCH_CMD   "searchBestMatch"
#define SB_CMD                  "sb"
#define QUIT_CMD               "quit"

namespace nvUtil
{
    int     readStdinInteger();
    // Read space-separated integer sequence
    bool    readStdinIntegerSequence(std::vector<int> & intSequence);
    bool    readStdinCommand(std::string & commandName, std::vector<int> & intSequence);
    bool    readSearchCommandFromStream(std::ifstream & commandStream, std::string & commandName, std::vector<int> & sequence);  
    void    printSequenceOnStdout(const std::vector<int> & sequence);
    void    countOccurrencesInSequence(const std::vector<int> & sequence, std::map<int, int> & occurenceCountMap);
};

class nvHighResTimer
{
public:
    nvHighResTimer(const std::string & timerId);
    void    begin();
    void    end();
    void    pause();
    std::chrono::microseconds   getTotalTime() const;
    std::chrono::microseconds   getMinimumTime() const;
    std::chrono::microseconds   getMaximumTime() const;

private:
    std::string     _id;
    std::chrono::high_resolution_clock::time_point  _refTimePoint;
    std::chrono::microseconds  _totalTime;
    std::chrono::microseconds  _minimumDuration;
    std::chrono::microseconds  _maximumDuration;
};
#endif
