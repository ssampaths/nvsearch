#include "matrix.h"
#include  <iostream>
#include "matrixIndexer.h"

nvMatrix::nvMatrix(int rowCount, int columnCount, bool enableIndexing) : 
    _indexer(nullptr), 
    _rowCount(rowCount), 
    _columnCount(columnCount), 
    _currentRow(0)
{
    if (enableIndexing) _indexer = new nvMatrixIndexer;
    else _elements.reserve(rowCount * columnCount);
}

nvMatrix::~nvMatrix()
{
    delete _indexer;
}

int nvMatrix::operator()(int row, int column) const
{
    unsigned index = row * _rowCount + column;

    if (index >= _elements.size())
        return 0;

    return _elements.at(index);
}

void nvMatrix::setDimension(int nRows, int nColumns)
{
    (void)nRows; (void)nColumns;
}

bool nvMatrix::appendRow(const std::vector<int> & row)
{
    if (_indexer) {
        _indexer->appendRow(_currentRow, row);
    }
    else {
        if (_columnCount != row.size() || 
                (_elements.size() + row.size()) > (_rowCount * _columnCount)) return false;

        _elements.insert(_elements.end(), row.begin(), row.end());
    }

    ++_currentRow;
    return true;
}

unsigned int nvMatrix::getRowCount() const
{
    return _rowCount;
}

unsigned int nvMatrix::getColumnCount() const
{
    return _columnCount;
}

const nvMatrixIndexer * nvMatrix::getIndexer() const
{
    return _indexer;
}
