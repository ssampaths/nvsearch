#ifndef _NV_DISK_ACCESS_H_
#define _NV_DISK_ACCESS_H_

#include <vector>
#include <fstream>
#include <memory>

class nvMatrix;

class nvDiskAccess 
{
public:
    virtual bool        writeMatrix(const nvMatrix & matrix) = 0;
    virtual std::unique_ptr<nvMatrix>  readMatrix(bool enableIndexing = false, bool dump = false) = 0;
    virtual bool        readSequence(int size, std::vector<int> & sequence) = 0;
};

class nvBinaryFormatDiskAccess : public nvDiskAccess
{
public:
    nvBinaryFormatDiskAccess(const char * fileName);
    ~nvBinaryFormatDiskAccess();
    virtual bool        writeMatrix(const nvMatrix & matrix) override;
    virtual std::unique_ptr<nvMatrix>  readMatrix(bool enableIndexing = false, bool dump = false) override;
    virtual bool        readSequence(int size, std::vector<int> & sequence) override;

private:
    std::fstream    _fileStream;
};

#endif
