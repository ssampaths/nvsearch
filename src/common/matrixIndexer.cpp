#include "matrixIndexer.h"


nvRowColumnPair::nvRowColumnPair(unsigned int row, unsigned int column) : _row(row), _column(column) 
{
}

bool nvRowColumnPair::operator< (const nvRowColumnPair & other) const
{
    if (this->_row == other._row) return (this->_column < other._column);
    return (this->_row < other._row);
}

void nvMatrixIndexer::appendRow(unsigned int rowNum, const std::vector<int> & row)
{
    if (row.empty()) return;

    for (size_t i = 0; i < row.size(); ++i) {
        _valueToPositionIndex.insert(std::pair<int, nvRowColumnPair>(row[i], nvRowColumnPair(rowNum, i)));
    }
}

std::pair<nvSearchIter, nvSearchIter> nvMatrixIndexer::findValue(int value) const  
{
    return _valueToPositionIndex.equal_range(value);
}
